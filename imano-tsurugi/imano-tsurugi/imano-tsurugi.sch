EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery BT1
U 1 1 5CD87719
P 3700 1500
F 0 "BT1" H 3808 1546 50  0000 L CNN
F 1 "Battery" H 3808 1455 50  0000 L CNN
F 2 "" V 3700 1560 50  0001 C CNN
F 3 "~" V 3700 1560 50  0001 C CNN
	1    3700 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1350 3700 1300
Wire Wire Line
	2300 1450 3000 1450
$Comp
L power:VCC #PWR?
U 1 1 5CD8F67A
P 950 1150
F 0 "#PWR?" H 950 1000 50  0001 C CNN
F 1 "VCC" H 967 1323 50  0000 C CNN
F 2 "" H 950 1150 50  0001 C CNN
F 3 "" H 950 1150 50  0001 C CNN
	1    950  1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CD90E09
P 950 2750
F 0 "#PWR?" H 950 2500 50  0001 C CNN
F 1 "GND" H 955 2577 50  0000 C CNN
F 2 "" H 950 2750 50  0001 C CNN
F 3 "" H 950 2750 50  0001 C CNN
	1    950  2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  1150 950  1250
Wire Wire Line
	950  1250 2850 1250
Wire Wire Line
	950  2750 950  2650
Wire Wire Line
	950  2650 2800 2650
Wire Wire Line
	3200 1500 3200 1250
Wire Wire Line
	3200 1800 3200 1950
$EndSCHEMATC
